import Header from "@/components/Header";
import PokemonChips from "@/components/PokemonChips";
import PokemonInfo from "@/components/PokemonInfo";
import LoadingPokemonInfo from "@/components/ui/LoadingPokemonInfo";
import LoadingPokemonsChips from "@/components/ui/LoadingPokemonsChips";

import { Box, Stack } from "@mui/material";
import { Suspense } from "react";

export default async function Home() {
  return (
    <Box
      component={"main"}
      py={"100px"}
      sx={{
        width: "100vw",
        height: "100vh",
        backgroundColor: "#131313",
        color: "white",
      }}
    >
      <Box sx={{ maxWidth: "1280px", margin: "0 auto", paddingX: "150px" }}>
        <Header />
        <Stack
          direction={"row"}
          columnGap={"12px"}
          mt={"54px"}
          height={500}
          alignItems={"center"}
        >
          <Box width={"50%"}>
            <Suspense fallback={<LoadingPokemonsChips />}>
              <PokemonChips />
            </Suspense>
          </Box>
          <Box width={"50%"} height={"100%"}>
            <Suspense fallback={<LoadingPokemonInfo />}>
              <PokemonInfo />
            </Suspense>
          </Box>
        </Stack>
      </Box>
    </Box>
  );
}
