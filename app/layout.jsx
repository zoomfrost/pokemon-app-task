import { Raleway } from "next/font/google";

import "@/app/main.css";
import CustomThemeProvider from "@/components/ui/CustomThemeProvider";

export const raleway = Raleway({
  subsets: ["latin", "cyrillic"],
  style: "normal",
});

export default function RootLayout({ children }) {
  return (
    <html lang="en" className={raleway.className}>
      <CustomThemeProvider>
        <body>{children}</body>
      </CustomThemeProvider>
    </html>
  );
}
