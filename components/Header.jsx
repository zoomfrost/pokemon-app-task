import { Box, Stack, Typography } from "@mui/material";
import Link from "next/link";
import React from "react";
import PokemonClick from "./ui/PokemonClick";

const Header = () => {
  return (
    <Stack direction={"row"} justifyContent={"space-between"}>
      <Box
        sx={{
          border: "1px solid white",
          padding: "7px",
          textTransform: "uppercase",
          textAlign: "center",
          fontSize: "12px",
          lineHeight: "13.52px",
          fontWeight: "500",
          textAlign: "center",
        }}
      >
        <Link style={{ textDecoration: "none", color: "white" }} href={"/"}>
          Покемоны APi
        </Link>
      </Box>

      <Box>
        <PokemonClick />
      </Box>
    </Stack>
  );
};

export default Header;
