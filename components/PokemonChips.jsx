import { Chip, Grid } from "@mui/material";
import axios from "axios";
import Link from "next/link";
import Error from "./ui/Error";

const PokemonChips = async () => {
  const baseUrl = "https://pokeapi.co/api/v2";
  const limit = 10;
  const data = await axios.get(`${baseUrl}/pokemon?limit=${limit}`);

  if (!data) return <Error />;

  const pokemonListData = data?.data.results.map((pokemonObj) => {
    return { name: pokemonObj.name };
  });

  return (
    <Grid columnSpacing={"6px"} rowSpacing={"10px"} container>
      {pokemonListData &&
        pokemonListData.map((item) => {
          return (
            <Grid key={item.name} item>
              <Link
                href={`?${new URLSearchParams({
                  pokemon: item.name,
                })}`}
              >
                <Chip
                  clickable
                  color="primary"
                  sx={{
                    height: "60px",
                    minWidth: "135px",
                    color: "white",
                    fontSize: "20px",
                    fontWeight: "500",
                    paddingY: "20px",
                    paddingX: "8px",
                    borderRadius: "44px",
                  }}
                  label={item.name}
                />
              </Link>
            </Grid>
          );
        })}
    </Grid>
  );
};

export default PokemonChips;
