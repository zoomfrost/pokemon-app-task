"use client";

import { Box, Stack, Typography } from "@mui/material";
import Image from "next/image";
import axios from "axios";
import { useSearchParams } from "next/navigation";
import LoadingPokemonInfo from "./ui/LoadingPokemonInfo";
import { useEffect, useState } from "react";
import Error from "./ui/Error";

const PokemonInfo = () => {
  const searchParams = useSearchParams();
  const pokemon = searchParams.get("pokemon");
  const [pokemonInfo, setPokemonInfo] = useState(null);
  const [error, setError] = useState(false);

  const baseUrl = "https://pokeapi.co/api/v2";

  useEffect(() => {
    if (pokemon) {
      setError(false);
      axios
        .get(`${baseUrl}/pokemon/${pokemon}`)
        .then((data) => setPokemonInfo(data.data))
        .catch(() => setError(true));
    }
  }, [pokemon]);

  if (!pokemon) {
    return null;
  }

  if (error) return <Error />;
  if (!pokemonInfo) return <LoadingPokemonInfo />;

  const pokemonData = {
    name: pokemonInfo.name,
    height: pokemonInfo.height,
    id: pokemonInfo.id,
    img: pokemonInfo.sprites.front_shiny,
    attack: pokemonInfo.stats[1].base_stat,
  };
  return (
    <Stack
      px={"44px"}
      pt={"44px"}
      pb={"16px"}
      color={"#A0A0A0"}
      height={500}
      width={484}
      bgcolor={"black"}
      textTransform={"capitalize"}
      fontFamily="raleway"
      direction={"column"}
      justifyContent={"space-between"}
    >
      <Typography fontSize="48px" lineHeight="48px" fontWeight={700}>
        {pokemonData.name}
      </Typography>
      <Box sx={{ position: "relative " }} height={"200px"} width="100%">
        <Image
          style={{ margin: "0 auto", objectFit: "contain" }}
          fill
          src={pokemonData.img}
          alt="pokemonImg"
        />
      </Box>
      <Box>
        <Typography fontSize="17px" variant="body1">
          Id: {pokemonData.id}
        </Typography>
        <Typography fontSize="17px" variant="body1">
          height: {pokemonData.height}
        </Typography>
        <Typography fontSize="17px" variant="body1">
          attack: {pokemonData.attack}
        </Typography>
      </Box>
    </Stack>
  );
};

export default PokemonInfo;
