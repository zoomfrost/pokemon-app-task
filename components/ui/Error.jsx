import { Stack, Typography } from "@mui/material";

const Error = () => {
  return (
    <Stack
      justifyContent="center"
      alignItems="center"
      width={"100%"}
      height={"100%"}
    >
      <Typography variant="h3">Произошла ошибка</Typography>
    </Stack>
  );
};

export default Error;
