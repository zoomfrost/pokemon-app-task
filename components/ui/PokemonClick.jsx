import { Stack, Typography } from "@mui/material";
import Image from "next/image";
import React from "react";

import ClickIcon from "@/public/Icon.svg";

const PokemonClick = () => {
  return (
    <Stack
      direction={"row"}
      columnGap={"10px"}
      alignItems={"center"}
      width={"190px"}
    >
      <Image src={ClickIcon} alt="click-icon" />
      <Typography lineHeight={"12px"} fontSize="12px" fontWeight={600}>
        Нажмите на <br /> нужного Покемона
      </Typography>
    </Stack>
  );
};

export default PokemonClick;
